#include <iostream>
#include <algorithm>
#include <vector>
#include <chrono>
#include <limits.h>
#include <stdio.h>
#include <omp.h>
#include <CL/cl.hpp>

/** @file */
/**
 * Calculates the minimal distance and returns the index of the closest vertex.
 * @param dist The distance vector.
 * @param verticesSet The vector of whether the connected vertex has been visited.
 * @param numberOfVertices How many vertices there are in dist.
 **/
int minDistance(int dist[], bool verticesSet[], int numberOfVertices)
{
    // Initialize min value
    int min = INT_MAX, min_index;

    for (int v = 0; v < numberOfVertices; v++)
        if (verticesSet[v] == false && dist[v] <= min)
            min = dist[v], min_index = v;

    return min_index;
}
/**
 * Calculates the minimal distance and returns the index of the closest vertex
 * in the given array space.
 * @param dist The distance vector.
 * @param verticesSet The vector of whether the connected vertex has been visited.
 * @param start Start index for traversal.
 * @param end End index for traversal.
 **/
int parallelMinDistance(int dist[], bool verticesSet[], int start, int end)
{
    // Initialize min value
    int min = INT_MAX, min_index = -1;

    for (int v = start; v <= end; v++)
        if (verticesSet[v] == false && dist[v] <= min && dist[v] != 0)
            min = dist[v], min_index = v;

    return min_index;
}

/**
 * Prints the vertices in the given vector with their respective distance from the source node.
 * @param dist The vector that is to be printed.
 * @param numberOfVertices How many vertices there are in the vector.
 **/
void printSolution(int dist[], int numberOfVertices)
{
    printf("Vertex \t\t Distance from Source\n");
    for (int i = 0; i < numberOfVertices; i++)
    {
        if (dist[i] == INT_MAX)
        {
            printf("%d \t\t Unreachable\n", i);
        }
        else
        {
            printf("%d \t\t %d\n", i, dist[i]);
        }
    }
}

/**
 * Prints the matrix that is provided.
 * @param graph The 2D int array graph.
 * @param numberOfVertices How many vertices there are in the graph.
 **/
void printAdjacencyMatrix(int **graph, int numberOfVertices)
{
    for (int i = 0; i < numberOfVertices; ++i)
    {
        for (int j = 0; j < numberOfVertices; j++)
        {
            std::cout << graph[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

/**
 * Simplified dijkstra algorithm using adjacency matrix.
 * The matrix is created in the function buildGraph.
 * The graph's array is dynamically allocated at runtime, which allows for user interaction.
 * @param graph The graph that is to be traversed.
 * @param src The source vertex index.
 * @param numberOfVertices The number of vertices in the graph that is passed.
 * @param flag Whether output is desired. --no-print for no output, anything else for output. 
 **/
void sequentialDijkstra(int **graph, int src, int numberOfVertices, std::string flag)
{

    auto start = omp_get_wtime();
    int dist[numberOfVertices];         // Distance vector from src.
    bool verticesSet[numberOfVertices]; // Bool vector if a vertex has been visited.

    // Initialize all distances as INT_MAX and all values in verticesSet[] as false
    for (int i = 0; i < numberOfVertices; i++)
        dist[i] = INT_MAX, verticesSet[i] = false;

    // Initialize source vertex distance as 0
    dist[src] = 0;

    // Loop over vertices and find shortest distance to src. Src is source node in the first iteration.
    for (int count = 0; count < numberOfVertices; count++)
    {

        // Get closest vertex.
        int u = minDistance(dist, verticesSet, numberOfVertices);

        // Set vertex at that index as visited.
        verticesSet[u] = true;

        // Update graph.
        // Add closest distance to a vertex' distance from src if:
        // 1) It has not been visited
        // 2) Its distance is greater than 0 (reachable from here)
        // 3) The closest distance to src is not INT_MAX away
        // 4) The distance to the closest vertex plus the distance at that vertex is lower than the current lowest dist value
        // from src for this vertex.
        for (int v = 0; v < numberOfVertices; v++)
            if (!verticesSet[v] && graph[u][v] && dist[u] != INT_MAX && dist[u] + graph[u][v] < dist[v])
                dist[v] = dist[u] + graph[u][v];
    }
    auto end = omp_get_wtime();
    printf("Sequential execution took %f milliseconds\n", (end - start) * 1000);

    if (flag != "--no-print")
        printSolution(dist, numberOfVertices);
}

/**
 * Simple implementation of parallel dijkstra.
 * @param graph The 2D graph array.
 * @param src The index of the source vertex.
 * @param numberOfVertices How many vertices are in the provided graph.
 * @param flag Whether output is desired. --no-print for no output, anything else for output.
 **/
void simpleParallelDijkstra(int **graph, int src, int numberOfVertices, std::string flag)
{
    auto start = omp_get_wtime();

    int dist[numberOfVertices];         // Distance vector from src.
    bool verticesSet[numberOfVertices]; // Bool vector if a vertex has been visited.

    // Initialize all distances as next step and all values in verticesSet[] as false
    for (int i = 0; i < numberOfVertices; i++)
    {
        if (graph[src][i] > 0)
        {
            dist[i] = graph[src][i], verticesSet[i] = false;
        }
        else
        {
            dist[i] = INT_MAX, verticesSet[i] = false;
        }
    }

    // Initialize source vertex distance as 0
    dist[src] = 0;

    /**
     * Begin parallel processes. 
     * 1) Seperate problem space into <number of cores> chunks.
     * 2) Find local closest for each chunk.
     * 3) Find global closest overall.
     * 4) Relax graph for each chunk.
    **/
    // Loop over vertices and find shortest distance to src. Src is source node in the first iteration.

    int currentGlobalMin, currentGlobalMinVertex = -1;
    currentGlobalMin = INT_MAX;
    int numberOfThreads = omp_get_num_procs();
    int u;
    int me;
    int sizeOfChunks = numberOfVertices / numberOfThreads;
    int startv;
    int endv;

#pragma omp for
    for (int i = 0; i < numberOfVertices; i++)
    {
        currentGlobalMin = INT_MAX;
        currentGlobalMinVertex = -1;
#pragma omp parallel private(u, me, startv, endv) \
    shared(dist, verticesSet)
        {
            me = omp_get_thread_num();
            startv = me * sizeOfChunks;

            // Last chunk handles the rest.
            if (me == (numberOfThreads - 1))
                endv = numberOfVertices - 1;
            else
                endv = startv + sizeOfChunks - 1;

            // Each chunks finds their minimum distance.
            // Find local min.

            // Chunk size 1.

            u = parallelMinDistance(dist, verticesSet, startv, endv);

            // Find global min
            if (dist[u] < currentGlobalMin && u != src && u != -1 && !verticesSet[u])
            {
                currentGlobalMin = dist[u];
                currentGlobalMinVertex = u;
            }

#pragma omp barrier
#pragma omp single
            {
                // Set vertex at that index as visited.
                if (currentGlobalMinVertex != -1)
                {
                    verticesSet[currentGlobalMinVertex] = true;
                }
            }

            // Update graph for all chunks parallely on shared graph and dist.
            // Each thread only updates its own chunk.
            // Add closest distance to a vertex' distance from src if:
            // 1) It has not been visited
            // 2) It's distance is greater than 0
            // 3) The closest distance to src is not INT_MAX away
            // 4) The distance to the closest vertex plus the distance at that vertex is lower than the current lowest dist value
            // from src for this vertex.
            if (currentGlobalMinVertex != -1)
            {
                if (startv == endv)
                {
                    if (!verticesSet[startv] && graph[currentGlobalMinVertex][startv] && currentGlobalMin != INT_MAX && currentGlobalMinVertex != -1 && dist[currentGlobalMinVertex] + graph[currentGlobalMinVertex][startv] < dist[startv])
                        dist[startv] = dist[currentGlobalMinVertex] + graph[currentGlobalMinVertex][startv];
                }
                else
                    for (int v = startv; v <= endv; v++)
                    {
                        if (!verticesSet[v] && graph[currentGlobalMinVertex][v] && currentGlobalMin != INT_MAX && currentGlobalMinVertex != -1 && dist[currentGlobalMinVertex] + graph[currentGlobalMinVertex][v] < dist[v])
                            dist[v] = dist[currentGlobalMinVertex] + graph[currentGlobalMinVertex][v];
                    }
            }
        }
    }

    // Get time
    auto end = omp_get_wtime();
    printf("Parallel execution with OpenMP took %f milliseconds\n", (end - start) * 1000);

    if (flag != "--no-print")
        printSolution(dist, numberOfVertices);
}

/**
 * Parallel dijkstra with OpenCL.
 * @param graph The 2D graph array.
 * @param src The index of the source vertex.
 * @param numberOfVertices How many vertices are in the provided graph.
 * @param flag Whether output is desired. --no-print for no output, anything else for output.
 **/
void openclParallelDijkstra(int **graph, int src, int numberOfVertices, std::string flag)
{
    auto t1 = std::chrono::steady_clock::now();

    /**
     * OpenCl setup.
     */
    //get all platforms (drivers)
    std::vector<cl::Platform> all_platforms;
    cl::Platform::get(&all_platforms);
    if (all_platforms.size() == 0)
    {
        std::cout << " No platforms found. Check OpenCL installation!\n";
        exit(1);
    }
    cl::Platform default_platform = all_platforms[0];
    std::cout << "Using platform: " << default_platform.getInfo<CL_PLATFORM_NAME>() << "\n";

    //get default device of the default platform
    std::vector<cl::Device> all_devices;
    default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
    if (all_devices.size() == 0)
    {
        std::cout << " No devices found. Check OpenCL installation!\n";
        exit(1);
    }
    cl::Device default_device = all_devices[0];
    std::cout << "Using device: " << default_device.getInfo<CL_DEVICE_NAME>() << "\n";

    // Create context.
    cl::Context context({default_device});

    // Program source.
    cl::Program::Sources sources;

    // Kernel source, calculates minimum distance in segment and relaxes graph.
    std::string kernel_code =
        "   void kernel min_distance(global int* dist, global bool* verticesSet, const int sizeOfChunks, global int* result, const int huge_int, global int* graph, const int numberOfVertices) {\n"
        "       for (int b = 0; b < numberOfVertices; b++) {\n"
        "           int gid = get_global_id(0);\n"
        "           int min = huge_int, min_index = -1;\n "
        "           for (int v = gid * sizeOfChunks; v < sizeOfChunks * gid + sizeOfChunks; v++) {\n"
        "               if (verticesSet[v] == false && dist[v] < min && dist[v] != 0) {\n"
        "                   min = dist[v];\n"
        "                   min_index = v;\n"
        "                }\n"
        "           }\n"
        "           result[gid] = min_index;\n"
        "           if (gid != 0) continue;"
        "           min = huge_int;\n"
        "           min_index = -1;\n"
        "           int current_min;"
        "           for (int a = 0; a < numberOfVertices; a++) {\n"
        "               current_min = dist[result[a]];"
        "               if (current_min < min && current_min != -1 && current_min != 0) { min = current_min; min_index = result[a]; }\n"
        "           }"
        "           verticesSet[min_index] = true;"
        "" // relax graph with found global min.
        "           int a = 0;"
        "           int min_dist = dist[min_index];"
        "           int current_dist;"
        "           int compare_dist;"
        "           for (int i = min_index * numberOfVertices; i < min_index * numberOfVertices + numberOfVertices; i++) {\n"
        "               current_dist = dist[a];"
        "               compare_dist = graph[min_index * numberOfVertices + a];"
        "               if (current_dist > min_dist + compare_dist && !verticesSet[a] && compare_dist != 0) {\n"
        "                   dist[a] = min_dist + compare_dist;\n"
        "               }"
        "               a++;"
        "           }"
        "       }\n"
        "   }\n";
    sources.push_back({kernel_code.c_str(), kernel_code.length()});

    cl::Program program(context, sources);
    if (program.build({default_device}) != CL_SUCCESS)
    {
        std::cout << " Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) << "\n";
        exit(1);
    }

    // create buffers on the device.
    // Buffers are created for each segment of the dist vector. Number of these buffers
    // depends on the number of compute units.
    int numberOfComputeUnits;
    if (numberOfVertices < (int)default_device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>())
    {
        numberOfComputeUnits = numberOfVertices;
    }
    else
    {
        numberOfComputeUnits = default_device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
    }
    int sizeOfChunks = (numberOfVertices / numberOfComputeUnits);
    cl::Buffer result_buffer = cl::Buffer(context, CL_MEM_READ_WRITE, numberOfComputeUnits * sizeof(int));
    // Result buffer
    std::vector<int *> array_list; // Array list for host.

    int dist_array[numberOfVertices]; // Distance vector from src. Is seperated into
    // work items.
    cl::Buffer dist_buffer = cl::Buffer(context, CL_MEM_READ_WRITE, numberOfVertices * sizeof(int));
    int *reducedGraph = (int *)malloc(numberOfVertices * numberOfVertices * sizeof(int *)); // 2d graph reduced to 1d structure for OpenCL.
    int *preliminaryResult = new int[numberOfVertices];                                     // Global result vertex index. Is updated by all threads.
    bool verticesSet[numberOfVertices];                                                     // Bool vector if a vertex has been visited.
    // Initialize all distances as next step and all values in verticesSet[] as false
    cl::Buffer verticesBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, numberOfVertices * sizeof(bool)); // Visited vertices buffer.

    // Prepare result vector.
    for (int i = 0; i < numberOfVertices; i++)
    {
        if (graph[src][i] > 0)
        {
            dist_array[i] = graph[src][i];
        }
        else
        {
            dist_array[i] = INT_MAX, verticesSet[i] = false;
        }
        verticesSet[i] = false;
        preliminaryResult[i] = -1;
    }
    verticesSet[src] = true;

    // Reduce graph to 1d.
    // The new structure of the graph is such that every row of the graph is added to the array.
    // Each row is accessible with an offset of numberOfVertices * rowNumber.
    // Each element is accessible with offset + columnNumber. This is important for relaxation.
#pragma omp for
    for (int q = 0; q < numberOfVertices; q++)
    {
        for (int t = 0; t < numberOfVertices; t++)
        {
            reducedGraph[q * numberOfVertices + t] = graph[q][t];
        }
    }

    cl::Buffer graphBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, numberOfVertices * numberOfVertices * sizeof(int));

    // Create queue to which we will push commands for the device.
    cl::CommandQueue queue(context, default_device);

    // Write arrays.
    queue.enqueueWriteBuffer(dist_buffer, 0, 0, numberOfVertices * sizeof(int), dist_array);
    queue.enqueueWriteBuffer(verticesBuffer, 0, 0, numberOfVertices * sizeof(int), verticesSet);
    queue.enqueueWriteBuffer(graphBuffer, 0, 0, numberOfVertices * numberOfVertices * sizeof(int), reducedGraph);

    // Result array.
    queue.enqueueWriteBuffer(result_buffer, CL_FALSE, 0, sizeof(int) * numberOfVertices, preliminaryResult);

    cl::Kernel kernel_dijkstra = cl::Kernel(program, "min_distance");

    // Set arguments.
    kernel_dijkstra.setArg(0, dist_buffer);
    kernel_dijkstra.setArg(1, verticesBuffer);
    kernel_dijkstra.setArg(2, sizeOfChunks);
    kernel_dijkstra.setArg(3, result_buffer);
    kernel_dijkstra.setArg(4, INT_MAX);
    kernel_dijkstra.setArg(5, graphBuffer);
    kernel_dijkstra.setArg(6, numberOfVertices);

    queue.enqueueNDRangeKernel(kernel_dijkstra, 0, cl::NDRange(numberOfVertices), numberOfComputeUnits);
    queue.enqueueReadBuffer(dist_buffer, false, 0, sizeof(int) * numberOfVertices, dist_array);
    dist_array[src] = INT_MAX;

    // Finish queue.
    cl_int err_queue = queue.finish();
    auto t3 = std::chrono::steady_clock::now();
    auto duration2 = std::chrono::duration_cast<std::chrono::milliseconds>(t3 - t1).count();
    std::cout << "Parallel execution w/ OpenCl took " << duration2 << " milliseconds." << std::endl;
    if (err_queue)
        std::cout << "Queue finish result error: " << err_queue << std::endl;

    queue.enqueueReadBuffer(dist_buffer, false, 0, sizeof(int) * numberOfVertices, dist_array);
    dist_array[src] = INT_MAX;

    if (flag != "--no-print")
    {
        std::cout << "Result: " << std::endl;
        printSolution(dist_array, numberOfVertices);
    }
    free(reducedGraph);
    free(preliminaryResult);
}

/**
 * Creates a graph's adjacency matrix with random values
 * @param numberOfVertices Number of vertices in the graph.
 * @param maxWeight The maximum weight in an edge. Minimum is 0.
 * @param zeroChance Probability that an edge has the weight 0 (not connected).
 * @param directed Whether the graph is directed (true) or undirected (false).
 * @param flag --no-print if no output is desired, any other flag if it should print.
 **/
int **buildGraph(int numberOfVertices, int maxWeight, int zeroChance, bool directed, std::string flag)
{
    auto t1 = std::chrono::steady_clock::now();
    int **graph = new int *[numberOfVertices];
    for (int i = 0; i < numberOfVertices; ++i)
    {
        graph[i] = new int[numberOfVertices];
    }

#pragma omp for
    for (int i = 0; i < numberOfVertices; ++i)
    {
        for (int j = 0; j < numberOfVertices; ++j)
        {
            if (i == j)
            {
                graph[i][j] = 0;
            }
            else
            {
                int tmp;
                if ((rand() % 100 + 1) > zeroChance)
                {
                    tmp = rand() % maxWeight + 1;
                }
                else
                {
                    tmp = 0;
                }
                if (!directed)
                {
                    graph[j][i] = tmp;
                }
                graph[i][j] = tmp;
            }
        }
    }
    if (flag != "--no-print")
    {
        printAdjacencyMatrix(graph, numberOfVertices);
    }
    auto t2 = std::chrono::steady_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
    std::cout << "Graph building took " << duration << " milliseconds." << std::endl;

    return graph;
}
