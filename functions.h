#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <CL/cl.hpp>
#include <omp.h>

/** @file */

/**!
 * \Creates a graph's adjacency matrix with random values
 * @returns A 2D integer array of a graph's adjacency matrix.
 **/
int **buildGraph(int numberOfVertices, int maxWeight, int zeroChance, bool directed, std::string flag);

void printSolution(int dist[]);

void printAdjacencyMatrix(int **graph);

int minDistance(int dist[], bool sptSet[]);

int parallelMinDistance(int dist[], bool verticesSet[], int start, int end);

void sequentialDijkstra(int **graph, int src, int numberOfVertices, std::string flag);

void simpleParallelDijkstra(int **graph, int src, int numberOfVertices, std::string flag);

void openclParallelDijkstra(int **graph, int src, int numberOfVertices, std::string flag);

#endif
