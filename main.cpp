#define CL_USE_DEPRECATED_OPENCL_2_0_APIS

#include "functions.h"
#include <CL/cl.hpp> /* read cpp_wrapper_fix.txt */

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <omp.h>

/*! \mainpage Parallel Systems - Shortest Path
 * 
 * \section intro_sec Introduction
 * 
 * This is the documentation for the SoSe 2020 Parallel Systems class project by Martin Neumann.
 * 
 * The topic is dijkstra's shortest path algorithm.
 * 
 * \section Compilation
 * 
 * Use the provided Makefile:
 * `$ make`
 * 
 * \section Usage
 * 
 * Type ./pasys for usage instructions.
 * 
 * ./pasys <number of vertices> <maximum edge weight> <chance that an edge is not connected> <source vertex> <directed> <opencl> <print>
 * 
 * Example: ./pasys 24 5 50 0 1 1 --print
 * 
 * This means:
 * 
 * \* Build a graph with 24 vertices.
 * \* Maximum possible edge weight is 5.
 * \* Probability that an edge is not connected to each of the other edges is 50%.
 * \* The source vertex is vertex 0.
 * \* The graph is undirected (1).
 * \* OpenCL is used.
 * \* The graph and results are going to be printed (use --no-print otherwise).
 */

int main(int argc, const char *argv[])
{

	/**
     * OpenCL initilization.
     **/
	/*
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);

	auto platform = platforms.front();
	std::vector<cl::Device> devices;
	platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);

	auto device = devices.front();
	auto vendor = device.getInfo<CL_DEVICE_VENDOR>();
	auto version = device.getInfo<CL_DEVICE_VERSION>();
	*/

	srand((unsigned int)time(NULL));

	/*
	 * OpenCl device discovery by Doğukan Çağatay
	 * https://gist.github.com/dogukancagatay/8419284 
	 */
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);

	int platform_id = 0;
	int device_id = 0;
	uint compute_units = 0;

	std::cout << "Number of Platforms: " << platforms.size() << std::endl;

	for (std::vector<cl::Platform>::iterator it = platforms.begin(); it != platforms.end(); ++it)
	{
		cl::Platform platform(*it);

		std::cout << "Platform ID: " << platform_id++ << std::endl;
		std::cout << "Platform Name: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;
		std::cout << "Platform Vendor: " << platform.getInfo<CL_PLATFORM_VENDOR>() << std::endl;

		std::vector<cl::Device> devices;
		platform.getDevices(CL_DEVICE_TYPE_GPU | CL_DEVICE_TYPE_CPU, &devices);

		for (std::vector<cl::Device>::iterator it2 = devices.begin(); it2 != devices.end(); ++it2)
		{
			cl::Device device(*it2);

			std::cout << "\tDevice " << device_id++ << ": " << std::endl;
			std::cout << "\t\tDevice Name: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
			std::cout << "\t\tDevice Type: " << device.getInfo<CL_DEVICE_TYPE>();
			std::cout << " (GPU: " << CL_DEVICE_TYPE_GPU << ", CPU: " << CL_DEVICE_TYPE_CPU << ")" << std::endl;
			std::cout << "\t\tDevice Vendor: " << device.getInfo<CL_DEVICE_VENDOR>() << std::endl;
			std::cout << "\t\tDevice Max Compute Units: " << device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << std::endl;
			if (device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() > compute_units)
				compute_units = device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
			std::cout << "\t\tDevice Global Memory: " << device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() << std::endl;
			std::cout << "\t\tDevice Max Clock Frequency: " << device.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>() << std::endl;
			std::cout << "\t\tDevice Max Allocateable Memory: " << device.getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>() << std::endl;
			std::cout << "\t\tDevice Local Memory: " << device.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>() << std::endl;
			std::cout << "\t\tDevice Available: " << device.getInfo<CL_DEVICE_AVAILABLE>() << std::endl;
		}
		std::cout << std::endl;
	}

	std::vector<std::string> allArgs(argv, argv + argc);

	if (argc != 8)
	{
		std::cout << "Usage: ./pasys <number of vertices> <maximum edge weight> <chance that an edge is not connected> <source vertex> <directed> <opencl> <print>" << std::endl
				  << "Example: ./pasys 10 5 50 0 1 1 --print" << std::endl
				  << "Number of vertices must be divisible by the max number of compute units (" << compute_units << ")." << std::endl
				  << "OpenCL might crash with large graphs." << std::endl;
		return 0;
	}

	std::string flag;
	int nodes = stoi(allArgs[1]);
	int maxWeight = stoi(allArgs[2]);
	int zeroChance = stoi(allArgs[3]);
	int sourceVertex = stoi(allArgs[4]);
	int directed = stoi(allArgs[5]);
	flag = allArgs[7];
	int opencl = stoi(allArgs[6]);

	if (nodes % compute_units != 0 && opencl)
	{
		std::cerr << "*** Error (OpenCL)***: Number of nodes must be divisible by number of compute units." << std::endl
				  << std::endl;
		std::cout << "Usage: ./pasys <number of vertices> <maximum edge weight> <chance that an edge is not connected> <source vertex> <directed> <opencl> <print>" << std::endl
				  << "Example: ./pasys 10 5 50 0 1 1 --print" << std::endl
				  << "Number of vertices must be divisible by the max number of compute units (" << compute_units << ")." << std::endl
				  << "OpenCL might crash with large graphs." << std::endl;
		return 0;
	}

	if (nodes <= 1 || maxWeight < 1 || zeroChance > 99 || zeroChance < 1)
	{
		std::cout << "Usage: ./pasys <number of vertices> <maximum edge weight> <chance that an edge is not connected> <source vertex> <directed> <opencl> <print>" << std::endl
				  << "Example: ./pasys 10 5 50 0 1 1 --print" << std::endl;
		return 0;
	}

	std::cout << "Building graph with " << nodes << " Nodes." << std::endl;

	/**
     * Build graph using adjacency matrix.
     **/
	int **graph = buildGraph(nodes, maxWeight, zeroChance, directed, flag);

	/**
    * Sequential dijsktra.
    **/
	std::cout << "Calculating distances with sequential algorithm." << std::endl;
	sequentialDijkstra(graph, sourceVertex, nodes, flag);

	/**
	 * Simple parallel dijkstra
	 **/
	std::cout << "Calculating distances with OpenMP." << std::endl;
	simpleParallelDijkstra(graph, sourceVertex, nodes, flag);

	/**
	 * OpenCl dijkstra.
	 **/
	if (opencl)
	{
		std::cout << "Calculating distances with OpenCL." << std::endl;
		openclParallelDijkstra(graph, sourceVertex, nodes, flag);
	}
	free(graph);
	return 0;
}
