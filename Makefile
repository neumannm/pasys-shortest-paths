CXX=g++ -fopenmp
CXXFLAGS=-g -std=c++11 -W -Wall
LIBS= -lOpenCL

pasys: main.o functions.o
	${CXX} ${CSSFLAGS}  -o pasys main.o functions.o ${LIBS}

main.o: main.cpp

functions.o: functions.cpp functions.h

clean: 
	@rm *.o pasys