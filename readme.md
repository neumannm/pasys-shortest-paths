# Class project for the 2020 summer semester parallel systems course

WiSe 2020 project by Martin Neumann for HTW Berlin.

# Compilation

Use the provided Makefile:

`$ make`

# Usage

Type `./pasys` to get detailed instructions. Hint: Use "--no-print" at the end if print of results to stdout is undesired. Flag must be provided. If print is desired, provide "--print" (or any other string).

./pasys <number of vertices> <maximum edge weight> <chance that an edge is not connected> <source vertex> <directed> <opencl> <print>
  
Example: ./pasys 24 5 50 0 1 1 --print

# Features

This program generates a randomized graph with the provided attributes and finds the shortest distances from a source node.
It uses a sequential, a parallel OpenMP and an OpenCL approach.

The program is not optimized and runs rather slow.

# More documentation

Find more information in the `refman.pdf` documentation file or the HTML documentation in the `/html` folder.

