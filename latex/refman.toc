\contentsline {chapter}{\numberline {1}Parallel Systems -\/ Shortest Path}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Compilation}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Usage}{1}{section.1.3}
\contentsline {chapter}{\numberline {2}Class project for the 2020 summer semester parallel systems course}{3}{chapter.2}
\contentsline {chapter}{\numberline {3}File Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}File List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}File Documentation}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}functions.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{7}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Function Documentation}{7}{subsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.1}build\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Graph()}{8}{subsubsection.4.1.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.2}min\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Distance()}{8}{subsubsection.4.1.1.2}
\contentsline {subsubsection}{\numberline {4.1.1.3}opencl\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Parallel\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Dijkstra()}{8}{subsubsection.4.1.1.3}
\contentsline {subsubsection}{\numberline {4.1.1.4}parallel\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Min\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Distance()}{9}{subsubsection.4.1.1.4}
\contentsline {subsubsection}{\numberline {4.1.1.5}print\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Adjacency\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Matrix()}{9}{subsubsection.4.1.1.5}
\contentsline {subsubsection}{\numberline {4.1.1.6}print\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Solution()}{9}{subsubsection.4.1.1.6}
\contentsline {subsubsection}{\numberline {4.1.1.7}sequential\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Dijkstra()}{11}{subsubsection.4.1.1.7}
\contentsline {subsubsection}{\numberline {4.1.1.8}simple\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Parallel\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Dijkstra()}{11}{subsubsection.4.1.1.8}
\contentsline {section}{\numberline {4.2}functions.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{12}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Function Documentation}{12}{subsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.1.1}build\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Graph()}{12}{subsubsection.4.2.1.1}
\contentsline {subsubsection}{\numberline {4.2.1.2}opencl\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Parallel\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Dijkstra()}{13}{subsubsection.4.2.1.2}
\contentsline {subsubsection}{\numberline {4.2.1.3}parallel\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Min\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Distance()}{13}{subsubsection.4.2.1.3}
\contentsline {subsubsection}{\numberline {4.2.1.4}sequential\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Dijkstra()}{14}{subsubsection.4.2.1.4}
\contentsline {subsubsection}{\numberline {4.2.1.5}simple\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Parallel\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Dijkstra()}{14}{subsubsection.4.2.1.5}
\contentsline {chapter}{Index}{15}{section*.11}
